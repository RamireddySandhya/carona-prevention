package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IPatientList;
import dao.PatientListImp;
import model.PatientList;


@WebServlet("/remove")
public class PatientRemove extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long adhar = Long.parseLong(request.getParameter("Adharnumber"));
		IPatientList lopImpl = new PatientListImp();
		PatientList lop = new PatientList();
	    lop.setAdharNumber(adhar);
	    int result = lopImpl.removeRecoveryPatient(lop);
	    if (result == 1) {
			

		request.getRequestDispatcher("patient-removed.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Patient details are not removed successfully!!!");
			request.getRequestDispatcher("remove.jsp").forward(request, response);
		}
	}

}
