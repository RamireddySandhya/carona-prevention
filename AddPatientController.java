package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IPatientList;
import dao.PatientListImp;
import model.PatientList;

@WebServlet("/list")
public class AddPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Long adhar = Long.parseLong(request.getParameter("AdharNumber"));
		String name = request.getParameter("name");
		String bg = request.getParameter("bloodgroup");
		String status = request.getParameter("status");
		String district = request.getParameter("District");
		Long mobile = Long.parseLong(request.getParameter("mobilenumber"));

		IPatientList lopImpl = new PatientListImp();
		PatientList Lop = new PatientList();
		Lop.setAdharNumber(adhar);
		Lop.setName(name);
		Lop.setBloodGroup(bg);
		Lop.setStatus(status);
		Lop.setDistrict(district);
		Lop.setMobile(mobile);
		int result = lopImpl.addnewpatient(Lop);
		if (result == 1) {
			HttpSession session = request.getSession(false);
			request.setAttribute("msg", name);

			request.getRequestDispatcher("success.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Patient details are not added successfully!!!");
			request.getRequestDispatcher("add-patient.jsp").forward(request, response);
		}

	}
}
