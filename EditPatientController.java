package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IPatientList;
import dao.PatientListImp;
import model.PatientList;

@WebServlet("/update")
public class EditPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Long adhar = Long.parseLong(request.getParameter("Adharnumber"));

		String status = request.getParameter("status");

		Long mobile = Long.parseLong(request.getParameter("mobilenumber"));

		IPatientList lopImpl = new PatientListImp();
		PatientList Lop = new PatientList();
		Lop.setAdharNumber(adhar);
		
		
		Lop.setStatus(status);
		
		Lop.setMobile(mobile);
		int result = lopImpl.editpatientdetails(Lop);
		if (result == 1) {
			HttpSession session = request.getSession(false);
			request.setAttribute("msg", adhar);

			request.getRequestDispatcher("updated.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Patient details are not updated successfully!!!");
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}

	}

}
