package dao;

import java.util.List;

import model.PatientList;

public interface IPatientList {
	public int addnewpatient(PatientList Lop);

	  public List<PatientList> viewPatientList();

	  public int editpatientdetails(PatientList Lop);

	  public int removeRecoveryPatient(PatientList Lop);
}
