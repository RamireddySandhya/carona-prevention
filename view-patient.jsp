<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view patients details</title>
</head>
<body>
	<h2  align="center">view patient details</h2>
	
	<hr/>
<p align="right">
<a href="index.jsp" >logOut</a>
</p>
	<table align="center" border=1>
		<tr>
			<th>AdharNumber:</th>
			<th>Name:</th>
			<th>Blood Group:</th>
			<th>Status:</th>
			<th>District:</th>
			<th>Mobile:</th>
		</tr>
		<c:forEach items="${List}" var="s">
			<tr>
				<td>${s.getAdharNumber()}</td>
				<td>${s.getName()}</td>
				<td>${s.getBloodGroup()}</td>
				<td>${s.getStatus()}</td>
				<td>${s.getDistrict()}</td>
				<td>${s.getMobile()}</td>
			</tr>

		</c:forEach>

	</table>

	
</body>
</html>
