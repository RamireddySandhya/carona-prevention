package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IPatientList;
import dao.PatientListImp;
import model.PatientList;

@WebServlet("/appear")
public class ViewPatientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IPatientList lopImpl = new PatientListImp();
		List<PatientList> List= lopImpl.viewPatientList();
		request.setAttribute("List", List);
		request.getRequestDispatcher("view-patient.jsp").forward(request, response);
	}

}
